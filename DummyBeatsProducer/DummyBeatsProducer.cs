﻿using System;
using System.Timers;
using static System.Math;

namespace rmm
{
    public class DummyBeatsProducer : IBeatsProducer
    {
        public override double SamplingRate => data.SamplingRate;
        public override double FrequencyDeviation => data.FrequencyDeviation;
        public override double MiddleFrequency => data.MiddleFrequency;
        public override double MinimumFrequency => data.MinimumFrequency;
        public override double MaximumFrequency => data.MaximumFrequency;
        public override double Period => data.Period;
        public override double NumOfChannels => data.NumOfChannels;

        public override event ChannelData DataReady;

        private Timer aTimer;
        private int CurrentChannelNum = 0;
        private void OnTimer(Object source, ElapsedEventArgs e)
        {
            double[] signal = new double[(int)(SamplingRate * Period)];
            for (int i = 0; i < signal.Length; i++)
            {
                signal[i] = Sin((double)i / ((CurrentChannelNum % 2 * 2) + 3) * PI);
            }
            DataReady(CurrentChannelNum++ % 2, signal);
        }
        public override object ShowConfigurationForm()
        {
            throw new NotImplementedException();
        }

        private DummyBeatsProducerP data;

        public DummyBeatsProducer()
        {
            data = new DummyBeatsProducerP(this);
            aTimer = new Timer(50);
            aTimer.Elapsed += OnTimer;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }
    }
}
