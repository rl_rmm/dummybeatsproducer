﻿using System;
using System.Collections.Generic;
using System.Text;

namespace rmm
{
    public class DummyBeatsProducerP
    {
        private double samplingRate;
        private double frequencyDeviation;
        private double middleFrequency;
        private double minimumFrequency;
        private double maximumFrequency;
        private double period;
        private double numOfChannels;
        public DummyBeatsProducerP(DummyBeatsProducer parent) => Parent = parent;
        private readonly DummyBeatsProducer Parent;

        public double SamplingRate { get => samplingRate; set => samplingRate = value; }
        public double FrequencyDeviation
        {
            get => frequencyDeviation; set
            {
                frequencyDeviation = value;
                minimumFrequency = middleFrequency - frequencyDeviation / 2;
                maximumFrequency = middleFrequency + frequencyDeviation / 2;
            }
        }

        public double MiddleFrequency
        {
            get => middleFrequency; set
            {
                middleFrequency = value;
                minimumFrequency = middleFrequency - frequencyDeviation / 2;
                maximumFrequency = middleFrequency + frequencyDeviation / 2;
            }
        }
        public double MinimumFrequency
        {
            get => minimumFrequency; set
            {
                minimumFrequency = value;
                frequencyDeviation = maximumFrequency - minimumFrequency;
                middleFrequency = minimumFrequency + frequencyDeviation / 2;
            }
        }
        public double MaximumFrequency
        {
            get => maximumFrequency; set
            {
                maximumFrequency = value;
                frequencyDeviation = maximumFrequency - minimumFrequency;
                middleFrequency = minimumFrequency + frequencyDeviation / 2;
            }
        }

        public double Period { get => period; set => period = value; }
        public double NumOfChannels { get => numOfChannels; set => numOfChannels = value; }
    }
}
